# LibreHealth toolkit

The lh-toolkit is a software API and interface which forms the basis to create health record systems. This forms the basis of LibreHealth where the users can create a customized medical records system with minimal or no programming knowledge. Since this has to be accessible to users from different departments  like researchers, clinicians, planners, epidemiologists and patients, the lh-toolkit can be considered as the core part of this project.

# Cloning

The lh-toolkit is one of the projects under the librehealth gruop in GitLab. It contains some of the most important software and interface required to co-ordinate with the other projects of LibreHealth  like librehealth-radiology, open-decision-framework and run efficiently. The table below can be refrred to understand the repositories present in lh-toolkit.


![](table.JPG)

As we can observe that this project should be opened under maven projects, else there is a possibility that all the models might not open correctly. Once the project is forked into the user's profile successfully, it is cloned to the local system easily using the git clone command. Many IDE's are available on the web and I preferred IntelliJ IDEA in this case considering its flexibility in handling complex projects like this.

## Using Github
The project can be cloned into any of the desired folder in local system using IntelliJ IDEA itself. After opening the IDE for first time, from the menu as shown below I selected the Check out from version control and then the GitHub option from the dropdown where the application asks for the URL of project repository in the user's GitHub account.

![](15.JPG)

From the image shown below, you can observe that a local directory is also needed to be defined to save the project.

![](16.JPG)

## Using Gitlab
The Git option can also be used to clone the project if you are using the link from gitlab and the whole process remains the same as before.

![](27.JPG)

By proceeding with the next steps, the project gets loaded into the IDE and it automatically shows the folders available and also Maven Projects which are useful in building and deployment. The maven projects on the right side of screen can be opened by selecting maven projects option by hovering over the icon on the bottom left of the screen. A remainder that should be considered while installation is that, the project should be selected as a maven project and there should be a relevant SDK installed on the system. In this case JDK 1.8 would suffice and the IDE detects everything by default once an updated version is available.
 ![](17.JPG)
 
 Another way to open the project is by cloning it into the local system using git commands and following the process shown as shown in the next chapter. This process is a lengthy one and requires user to manually select options to build the project. This above mentioned procedure is technically more simple and can save time. For users who require more understanding into the process of extraction, the procedure shown in next chapter is explained.