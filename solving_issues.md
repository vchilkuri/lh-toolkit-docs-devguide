# Solving Issues

All the installation which happens for the toolkit doesn't happen smoothly on a different version of a platform or a different environment. For example: Upon installation of the lh-toolkit in advanced mode using Postgre SQL, errors would pop up in the last stage. Such issues can be found under the 'issues' section in a particular project. 

If the user has an assigned role and access to the project, he/she can assign themselves to any issue buy selecting the 'assign yourself' option on the right side of the issue window.

The description of the issue will be given below and based on whether the issue is a technical or any handling issue with the project, corresponding steps can be followed by the developer to solve the issue. If the problems arise with the xml or any other files in the repository, any IDE like IntelliJ IDEA can be used to open the files and update/solve the issue. The verification whether an issue has been solved can be checked on the local system itself and corresponding files can be updated in the project on a secondary branch of the project using 'merge'. The files should'nt be updated to the master directly without the reviewing of others working on the project, especially in the open source environment as this.

A safer option would be to fork the project into the developer's own repository and work on the project in the IDE. The issues can be taken up by any number of users and the comments section is available to continue on the thread if any new issue arises or to discuss on the steps to be followed. The error shown each time the program is installed would also show the file and specific location of the error in case of lh-toolkit. A sample of errors tab in the lh-toolkit project is shown below.

![](33.JPG)

