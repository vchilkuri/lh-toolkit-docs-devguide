#Using the IntelliJ IDEA
IntelliJ IDEA is an IDE for Java, but it also understands and provides intelligent coding assistance for a variety of other languages like SQL, HTML, Javascript etc. I used IntelliJ IDEA because of its integrated version control systems and requirement of no additional plugins to equip it according to the project. IntelliJ IDEA can be used in our project to build the cloned repository. A basic version of IntelliJ IDEA is available for free on their website, this can be downloaded and used for our project. I used version 2016.2.4 which would be sufficient for the work to be done. After downloading the application the and opening it, an image as shown below appears on the screen.

![](1.JPG)

From the options available below, you can select the import project where it opens a new window.


![Selecting the directory](4.JPG)

Our project contains a pom.xml file and hence I opened it as a maven project. Once the directory is selected, it opens to another window as shown below.

![](5.JPG)

I selected the Import project from an external model and selected the maven option.

![](6.JPG)

The application automatically detects the directory and the type of dependencies in the project. The settings were automatically put to default according to the files. 

![](7.JPG)

I didn't import any other extra tests to incorporate into the project and in the next step, the application automatically detects relevant SDK, in this case I used the default Java 1.8 JDK available on my system. The following image shows how IDE takes in the SDK to run the project.

![](9.JPG)

And going through the next steps, I opened the lh-toolkit on the IDE.