# Summary

* [Introduction](README.md)
* [Cloning](methods.md)
* [Opening the Project](open_project.md)
* [Running the Project](running_the_project.md)
* [LibreHealth toolkit installation](librehealth_toolkit_installation.md)
* [Solving Issues](solving_issues.md)

